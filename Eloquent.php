<?php

namespace App\Repositories\Job;

use App\Events\CreateJobEvent;
use App\Notifications\Job\Laboratory\Estimation;
use App\Models\FindingType;
use App\Models\Job;
use App\Models\JobFinding;
use App\Models\JobLink;
use App\Models\TreatmentType;
use App\Models\User;
use App\Models\Chat;

/**
 * Class Eloquent
 *
 * @package App\Repositories\Job
 */
class Eloquent implements Repository
{
    /**
     * @var array
     */
    const FINDING_TYPE_METHOD = [
        'additional_treatments' => 'saveAdditionalTreatment',
        'findings' => 'saveFinding',
        'treatment_plan' => 'saveTreatmentPlan',
    ];

    /**
     * @var Job
     */
    protected $model;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var array
     */
    protected $data;

    /**
     * Eloquent constructor.
     * @param Job $job
     */
    public function __construct(Job $job)
    {
        $this->model = $job;
    }

    /**
     * @param $data
     * @param $user
     * @return mixed|void
     * @throws \Exception
     */
    public function store($data, $user)
    {
        $this->setUser($user)
            ->initData()
            ->replaceData($data)
            ->checkLaboratory()
            ->saveJob()
            ->saveJobFindings()
            ->createChat()
            ->sendLabProposal();

        broadcast(new CreateJobEvent)->toOthers();
    }

    /**
     * @return $this
     */
    private function createChat()
    {
        Chat::create([
            'job_id' => $this->model->id,
        ]);

        return $this;
    }

    /**
     * Check on Internal Laboratory (if id equal 0)
     *
     * @return $this
     */
    private function checkLaboratory()
    {
        if (isset($this->data['laboratory_id']) && (int) $this->data['laboratory_id'] === 0) {
            $this->data['laboratory_id'] = null;
        }

        return $this;
    }

    /**
     * @param $user
     * @return $this
     */
    private function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return $this
     * @throws \Exception
     */
    private function saveJob()
    {
        $this->model->fill($this->data)->save();

        return $this;
    }

    /**
     * @return $this
     */
    private function saveJobFindings()
    {
        foreach (self::FINDING_TYPE_METHOD as $type => $method) {
            foreach ($this->data[$type] as $tooth => $option) {
                if (empty($option)) {
                    continue;
                }

                call_user_func([$this, $method], $tooth, strtolower($option));
            }
        }

        return $this;
    }

    /**
     * @param $tooth
     * @param $options
     */
    public function saveAdditionalTreatment($tooth, $options)
    {
        foreach (array_unique(explode(',', $options)) as $option) {
            JobFinding::create([
                'is_additional' => JobFinding::IS_ADDITIONAL_YES,
                'tooth_number' => $tooth,
                'treatment_type_id' => TreatmentType::where('name', trim($option))->firstOrFail()->id,
                'created_by' => $this->user->id,
                'job_id' => $this->model->id
            ]);
        }
    }

    /**
     * @param $tooth
     * @param $option
     */
    public function saveTreatmentPlan($tooth, $option)
    {
        JobFinding::create([
            'is_additional' => JobFinding::IS_ADDITIONAL_NO,
            'tooth_number' => $tooth,
            'treatment_type_id' => TreatmentType::where('name', trim($option))->firstOrFail()->id,
            'created_by' => $this->user->id,
            'job_id' => $this->model->id
        ]);
    }

    /**
     * @param $tooth
     * @param $option
     */
    public function saveFinding($tooth, $option)
    {
        JobFinding::create([
            'is_additional' => JobFinding::IS_ADDITIONAL_NO,
            'tooth_number' => $tooth,
            'finding_type_id' => FindingType::where('name', trim($option))->firstOrFail()->id,
            'created_by' => $this->user->id,
            'job_id' => $this->model->id
        ]);
    }

    /**
     * @return $this
     * @throws \Exception
     */
    private function sendLabProposal()
    {
        if ((bool) ($this->data['send_lab_proposal'] ?? null)
                && isset($this->model->laboratory->email)) {
            $this->model->status = Job::STATUS_REQUEST;
            $this->model->save();
        }

        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    private function replaceData($data)
    {
        $this->data = array_replace($this->data, $data);

        return $this;
    }

    /**
     * @return $this
     */
    private function initData()
    {
        $this->data = [
            'additional_treatments' => [],
            'findings' => [],
            'treatment_plan' => [],
            'laboratory_id' => null,
            'framework_type_id' => null,
            'implant_type_id' => null,
            'patient_id' => '',
            'dentist_id' => $this->user->id,
            'office_id' => $this->user->offices()->first()->id ?? null,
            'insurance' => null,
            'estimated_cost' => [
                'from' => null,
                'to' => null,
            ],
            'laboratory_cost' => [],
            'additional_insurance' => 0,
            'denture_type_id' => null,
            'comment' => null,
            'discount' => 0,
            'aid' => null,
            'painting' => null,
            'impression' => null,
            'general_treatment' => [],
            'factor_management' => null,
            'status' => Job::STATUS_NEW,
            'send_lab_proposal' => false,
        ];

        return $this;
    }
}
