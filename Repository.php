<?php

namespace App\Repositories\Job;

/**
 * Interface JobRepository
 *
 * @package App\Repositories\Job
 */
interface Repository
{
    /**
     * @param $data
     * @param $user
     * @return mixed
     */
    public function store($data, $user);
}
