<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Job;

use function GuzzleHttp\Promise\inspect;

/**
 * Class JobsResource
 *
 * @package App\Http\Resources
 */
class JobsResource extends JsonResource
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $user = $request->user();
        $link = $this->link();

        return [
            'id' => $this->id,
            'created_at' => $this->created_at->format('d.m.Y H:i'),
            'office_name' => $this->office->name ?? '',
            'dentist_name' => $this->dentist->username ?? '',
            'patient_id' => $this->patient_id,
            'laboratory_name' => $this->laboratory->name ?? __('Internal Laboratory'),
            'laboratory_costs' => $this->formattedCost,
            'status_badge' => $this->status_badge,
            'status_name' => $this->status_name,
            'allow_laboratory_pdf' => $this->existsLaboratoryPdf(),
            'allow_resend_email' => $this->allowResendEmail(),
            'allow_print' => $this->isToBePrinted() && $this->existsLaboratoryPdf(),
            'allow_to_be_printed' => $user->isSuperAdmin() || $user->isRemote(),
            'allow_cancel' => !$this->isCanceled() && !$this->isDeclined(),
            'laboratory_pdf_actions' => $this->getAttachmentLinks(),
            'notes_action' => route('job.notes', ['job' => $this->id]),
            'resend_email_action' => route('job.show.proposal', ['job' => $this->id]),
            'print_action' => route('job.set.status', ['job' => $this->id, Job::STATUS_PRINTED]),
            'to_be_printed_action' => route('job.set.status', ['job' => $this->id, Job::STATUS_TO_BE_PRINTED]),
            'cancel_action' => route('job.set.status', ['job' => $this->id, Job::STATUS_CANCELED]),
            'unread_chat_messages_count' => $user->unreadMessagesByChat($this->chat ?? null)->count(),
            'chat' => $this->chat ?? null,
            'link' => [
                'expired_at' => !empty($link) ? (int) $link->expired_at->valueOf() : '',
                'created_at'  => !empty($link) ? (int) $link->created_at->valueOf() : '',
            ],
            'is_expired' => $this->isExpired(),
        ];
    }
}
