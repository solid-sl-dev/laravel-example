<?php

namespace App\Http\Controllers\Admin;

use App\Events\ChangeJobStatusEvent;
use App\Http\Requests\AdditionalModalBodyRequest;
use App\Http\Requests\FrameworkTypesRequest;
use App\Http\Requests\ImplantTypesRequest;
use App\Http\Requests\JobStoreRequest;
use App\Http\Requests\ResendProposalRequest;
use App\Http\Requests\TeethImageRequest;
use App\Http\Requests\TreatmentModalBodyRequest;
use App\Http\Requests\ValidateFindingsRequest;
use App\Models\DentureType;
use App\Models\FindingType;
use App\Models\ImplantType;
use App\Models\Job;
use App\Models\JobAttachment;
use App\Models\Laboratory;
use App\Models\Office;
use App\Models\Permission;
use App\Models\Unit;
use App\Models\User;
use App\Repositories\Job\Repository;
use App\Services\JobService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\JobsResource;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

/**
 * Class JobController
 *
 * @package App\Http\Controllers\Admin
 */
class JobController extends AdminController
{
    /**
     * @var string
     */
    public $controllerName = 'job';

    /**
     * @var Repository
     */
    private $repository;

    /**
     * JobController constructor.
     *
     * @param Repository $repository
     */
    public function __construct(Repository $repository)
    {
        $this->repository = $repository;

        parent::__construct();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        /**
         * @var User $user
         */
        $user = $request->user();
        $request->flash();
        $service = new JobService($user);

        return $this->view('index', [
            'laboratories' => $service->getUserLaboratories(),
            'offices' => $service->getUserOffices(),
            'dentists' => User::dentists(),
            'hasDentistPermission' => $user->hasPermission(Unit::SLUG_JOB_DENTIST . '.' . Permission::ACTION_SHOW),
            'hasOfficePermission' => $user->hasPermission(Unit::SLUG_JOB_OFFICE . '.' . Permission::ACTION_SHOW),
            'hasCreatePermission' => $user->hasPermission(Unit::SLUG_JOB . '.' . Permission::ACTION_CREATE),
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        return $this->view('create', [
            'dentists' => User::dentists($request->user()->offices()->first()),
            'findingTypes' => FindingType::all(),
            'implantTypes' => ImplantType::all(),
            'office' => $request->user()->offices()->first(),
        ]);
    }

    /**
     * @param JobStoreRequest $request
     * @return mixed
     */
    public function store(JobStoreRequest $request)
    {
        try {
            $this->repository->store($request->all(), $request->user());
            $data['code'] = JsonResponse::HTTP_OK;
            $data['message'] = __('Job Stored Successfully');
            $data['redirectUrl'] = route('admin');
        } catch (\Exception $exception) {
            $data['code'] = JsonResponse::HTTP_UNPROCESSABLE_ENTITY;
            $data['message'] = $exception->getMessage();
        }

        return response()->json(['data' => $data], $data['code']);
    }

    /**
     * @param Job $job
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Job $job)
    {
        $job->status = Job::STATUS_DECLINE;
        $job->save();

        return redirect()->back();
    }

    /**
     * @param Job $job
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notes(Job $job)
    {
        return $this->view('notes', ['model' => $job]);
    }

    /**
     * @param Job $job
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showProposal(Job $job)
    {
        abort_if(empty($job) || !$job->allowResendEmail(), 422, __('Invalid job status'));

        return $this->view('email.resend_email', [
            'job' => $job,
            'laboratories' => Laboratory::all(),
        ]);
    }

    /**
     * @param Job $job
     * @param ResendProposalRequest $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function resendProposal(Job $job, ResendProposalRequest $request)
    {
        $laboratory = Laboratory::find((int) $request->post('laboratory_id'));

        $job->laboratory_id = $laboratory->id;
        $job->status = Job::STATUS_REQUEST;
        $job->save();

        return $this->redirect('index');
    }

    /**
     * @param TeethImageRequest $request
     * @return JsonResponse
     * @throws \Facade\Ignition\Exceptions\InvalidConfig
     */
    public function getTeethImageUrl(TeethImageRequest $request)
    {
        $quarter = $request->post('quarter');
        $teeth = $request->post('teeth');
        $option = $request->post('option');
        $type = $request->post('type');

        return response()->json(['url' => teeth_service($quarter, $teeth, $option, $type)->getTeethImageUrl()]);
    }

    /**
     * @param AdditionalModalBodyRequest $request
     * @return mixed
     */
    public function getAdditionalModalBody(AdditionalModalBodyRequest $request)
    {
        $options = $request->post('options', []);

        if (empty($options['finding']) || empty($options['treatment'])) {
            throw new UnprocessableEntityHttpException(__('Invalid Parameters'));
        }

        return response()->json(['body' => getModalContent($options)]);
    }

    /**
     * @param TreatmentModalBodyRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function getTreatmentModalBody(TreatmentModalBodyRequest $request)
    {
        $options = $request->post('options');
        $dentureType = DentureType::find((int) $request->post('denture_type_id'));

        if (empty($dentureType) || empty($options['finding']) || !$dentureType->hasFindingOption($options['finding'])) {
            throw new UnprocessableEntityHttpException(__('Invalid Parameters'));
        }

        return response()->json(['body' => view('admin.job.partials.teethWizard.partials.treatment._modal_body', ['types' => getFormattedDentureOptions($dentureType->getOptionsByFinding($options['finding']))])->render()]);
    }

    /**
     * @param ImplantTypesRequest $request
     * @return JsonResponse
     */
    public function checkImplantTypes(ImplantTypesRequest $request)
    {
        $options = $request->post('options', []);

        return response()->json(['success' => checkImplantTypes($options)]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function validateData(Request $request)
    {
        $jobRequest = new JobStoreRequest();
        Validator::make($request->all(), $jobRequest->rules(), $jobRequest->messages())->validate();

        return response()->json(['success' => true]);
    }

    /**
     * @param JobAttachment $jobAttachment
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function laboratoryPdf(JobAttachment $jobAttachment)
    {
        return response()->download($jobAttachment->file_path);
    }

    /**
     * @param FrameworkTypesRequest $request
     *
     * @return JsonResponse
     */
    public function getFrameworkTypes(FrameworkTypesRequest $request)
    {
        $dentureType = DentureType::find($request->get('dentureType', 0));

        if (empty($dentureType)) {
            throw new UnprocessableEntityHttpException(__('Denture Type not found'));
        }

        return response()->json([
            'types' => $dentureType->frameworkTypes->pluck('name', 'id')]
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getList(Request $request)
    {
        return $this->getJobsList($request);
    }

    /**
     * @param Job $job
     * @param $status
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function setStatus(Job $job, $status, Request $request)
    {
        abort_if(!in_array((int) $status, Job::STATUSES), 422, __('Invalid Status'));

        $job->status = $status;
        $job->save();

        broadcast(new ChangeJobStatusEvent($job))->toOthers();

        return $this->getJobsList($request);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     */
    private function getJobsList(Request $request)
    {
        $service = new JobService($request->user(), $request->all(), $this->itemsPerPage);
        $jobs = $service->getPaginatedJobs();
        $jobs->setPath(route('job.get.list'));

        return response()->json([
            'list' => JobsResource::collection($jobs),
            'pagination' => $jobs->links()->render(),
            'job_ids' => $service->getAllJobIds(),
        ]);
    }

    /**
     * @param Request $request
     *
     * @param ValidateFindingsRequest $request
     * @return mixed
     */
    public function validateFindingOptions(ValidateFindingsRequest $request)
    {
        $options = array_unique(array_filter($request->get('options', [])));

        return response()->json([
            'success' => FindingType::whereIn('name', $options)->count() === count($options),
        ]);
    }
 }
